require('dotenv').config()
const { ApolloServer, gql } = require('apollo-server');
const typeDefs = require('./src/schemas')
const resolvers = require('./src/resolvers')
const isProd = process.env.STATUS === 'PRODUCTION'
const db = require('./db')

const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: !isProd,
  playground: !isProd,
});

server.listen().then((url: String) => {
  console.log(`🚀  Server ready at ${url}`);
});