import Entry from '../db';
const { Op } = require("sequelize");

export = {
  Query: {
    entries: async (parent: any, args: any, context: any, info: any) => {
      const { username } = args
      let request = {}

      if (username !== null && username !== '')
        request = {
          where: {
            username: {
              [Op.like]: username
            }
          }
        }

      const res = await Entry.findAll(request)
      return res
    }
  },
  Mutation: {
    addEntry: async (parent: any, args: any, context: any, info: any) => {
      const { username, sentence } = args
      const res = await Entry.findOrCreate({
        where: { username, sentence }
      })
      return res[0]
    },



    deleteEntry: (parent: any, args: any, context: any, info: any) => {
      const { id } = args
      return Entry.findOne({ where: { id } }).then(async (res) => {
        await Entry.destroy({ where: { id } })
        return res
      })
    }
  }
};