const { gql } = require('apollo-server')

const typeDefs = gql`
    type Entry {
        id: Int
        username: String
        sentence: String
        createdAt: String
    }

    type Query {
        entries(username: String): [Entry]!
    }

    type Mutation {
        addEntry(
            username: String!
            sentence: String!
        ): Entry

        deleteEntry(
            id: Int!
        ): Entry
    }
`;

export = typeDefs