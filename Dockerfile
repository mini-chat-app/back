FROM node:alpine
WORKDIR /back

ADD *.ts ./
ADD src ./src
ADD .env ./

ADD package.json ./
ADD tsconfig.json ./

RUN yarn install && yarn tsc

EXPOSE 4000
CMD [ "yarn", "start" ]