import * as Sequelize from 'sequelize';

const {
    DB_USER = 'postgres', 
    DB_PASSWORD = 'postgres',
    DB_HOST = '0.0.0.0', 
    DB_PORT = 5432, 
    DB = 'postgres' 
} = process.env

const db = new Sequelize.Sequelize(`postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB}`);

db.authenticate()
.then(()=>db.sync())
.catch((e)=>console.log(e))

const Entry = db.define('Entries', {
    username: { type: Sequelize.STRING },
    sentence: { type: Sequelize.STRING },
})

export = Entry