# Config
Create .env file at root
```
STATUS=TEST                     
    # if not set to PRODUCTION then activate introspection and playground

DB_USER=postgres
    # Default postgres

DB_PASSWORD=postgres
    # Default postgres

DB_HOST=0.0.0.0
    # Default 0.0.0.0
    # set to   db   if used with the docker-compose of infra

DB_PORT=5432
    # Default 5432

DB=postgres
    # Default postgres
```
# Install
```
yarn install
```

# Run
```
yarn start
``` 
```
nodemon
```
```
go to infra -> docker-compose up
-> goto http://localhost
```